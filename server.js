const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const {userModel} = require('./database/index')

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.get('/user', (req, res) => {
    userModel.findAll()
      .then((result) => {
       
        res.send({
          status: 200,
          data: result
        })
      })
      .catch((err) => console.log(err))
})

app.post('/user', (req, res) => {
    const { id } = req.body
    axios.get('https://swapi.dev/api/people/' + id)
    .then(result => {
        const { name, height, mass} = result.data
        userModel.create({name, height, mass})
        .then(result => res.send({
            status:200,
            data:result
        }))
        .catch(err =>{
            res.send({
                status:500,
                data:err
            })
        })
    })
})


app.listen(3002, () => {
    console.log('Server is running on port 3002')
})
